Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: geojson
Upstream-Contact: The GeoRust Developers <mods@georust.org>
Source: https://github.com/georust/geojson

Files: *
Copyright: 2010 The Rust Prject Developers
           The GeoRust Developers <mods@georust.org>
License: MIT or Apache-2.0
Comment: Cargo.toml mentions The GeoRust Developers as copyright holders whereas LICENSE mentions The Rust Project Developers.

Files: src/conversion/mod.rs
 src/feature.rs
 src/feature_collection.rs
 src/feature_iterator.rs
 src/geojson.rs
 src/geometry.rs
 src/lib.rs
 src/util.rs
Copyright: 2014-2015 The GeoRust Developers <mods@georust.org>
License: Apache-2.0

Files: tests/fixtures/canonical/*.geojson
Copyright: 2017 Mapbox
License: Expat
Comment: Taken from https://github.com/mapbox/geojsonhint/tree/master/test/data/good

Files: debian/*
Copyright:
 2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2023 Matthias Geiger <werdahias@riseup.net>
 2023 Arnaud Ferraris <aferraris@debian.org>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Expat
 Permission to use, copy, modify, and/or distribute this software for any 
 purpose with or without fee is hereby granted, provided that the above 
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.
